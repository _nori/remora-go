package rule

import (
	"remora-go/model/shiftSchedule/dao"
	"time"
)

type RuleConf interface {
	// 檢查休息日
	CheckOffDay(ss dao.ShiftSchedule) dao.RuleError
	// 檢查列假
	CheckOfficialHoliday(ss dao.ShiftSchedule) dao.RuleError
	// 檢查每週工時
	CheckWeekHour(ss dao.ShiftSchedule) dao.RuleError
	// 一日最高工作時數
	CheckDailyHour(ss dao.ShiftSchedule) dao.RuleError
}

type TwoWeekRule struct {
	Start time.Time
}

// 休假
func (twr *TwoWeekRule) CheckOffDay(ss dao.ShiftSchedule) dao.RuleError {
	// 每二周要有二天
	err := NewRuleError(ErrorOffDayNotEnouph, "off day not enouph")
	hasErr := false
	var curRange, myRange, day int
	offDays := 0
	for _, s := range ss {
		day = int(s.Start.Sub(twr.Start).Hours()/24) + 1
		myRange = day / 15
		if myRange != curRange {
			curRange = myRange
			offDays = 0
		}
		if day != 0 && day%14 == 0 {
			if s.Typ == dao.OffDay {
				offDays++
			}
			if offDays < 2 {
				s.SetErr(err)
				hasErr = true
			}
		}
		if s.Typ != dao.OffDay {
			continue
		}
		offDays++
	}
	if hasErr {
		return err
	}
	return nil
}

// 例假
func (twr *TwoWeekRule) CheckOfficialHoliday(ss dao.ShiftSchedule) dao.RuleError {
	// 每七天內要有一天例假
	preDay, day := 0, 0
	err := NewRuleError(ErrorOfficialHolidayNotEnouph, "official holiday over seven days")
	hasErr := false
	for _, s := range ss {
		if s.Typ != dao.OfficialHoliday {
			continue
		}
		day = int(s.Start.Sub(twr.Start).Hours()/24) + 1
		if day-preDay > 7 {
			s.SetErr(err)
			hasErr = true
		}
		preDay = day
	}
	if hasErr {
		return err
	}
	return nil
}

func (twr *TwoWeekRule) CheckWeekHour(ss dao.ShiftSchedule) dao.RuleError {
	// 每週工作時數不得超過48小時
	err := NewRuleError(ErrorOverWeekWorkHours, "over week work hours")
	hasErr := false
	var day int
	var workHours float64
	for _, s := range ss {
		day = int(s.Start.Sub(twr.Start).Hours()/24) + 1
		if s.Typ != dao.WorkDay {
			if day != 0 && day%7 == 0 {
				workHours = 0
			}
			continue
		}
		workHours += s.Interval.Hours()
		if workHours > 48 {
			hasErr = true
			s.SetErr(err)
		}
		if day != 0 && day%7 == 0 {
			workHours = 0
		}
	}
	if hasErr {
		return err
	}
	return nil
}

func (twr *TwoWeekRule) CheckDailyHour(ss dao.ShiftSchedule) dao.RuleError {
	// 每天工作不能超過12小時
	err := NewRuleError(ErrorDailyWorkHours, "daily hour over ten hour")
	hasErr := false
	var dateKey, key string
	var totalHour float64
	const dateFormat = "20060102"
	for _, s := range ss {
		if s.Typ != dao.WorkDay {
			continue
		}
		key = s.Start.Format(dateFormat)

		if dateKey != key {
			totalHour = 0
			dateKey = key
		}
		totalHour += s.Interval.Hours()
		if totalHour > 12 {
			s.SetErr(err)
			hasErr = true
		}
	}
	if hasErr {
		return err
	}
	return nil
}
