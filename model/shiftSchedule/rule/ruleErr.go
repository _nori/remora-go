package rule

import "remora-go/model/shiftSchedule/dao"

const (
	ErrorOffDayNotEnouph          = "100"
	ErrorOfficialHolidayNotEnouph = "101"
	ErrorDailyWorkHours           = "102"
	ErrorOverWeekWorkHours        = "103"
)

func NewRuleError(code, msg string) dao.RuleError {
	return &ruleErrImpl{
		code: code,
		msg:  msg,
	}
}

type ruleErrImpl struct {
	code string
	msg  string
}

func (re *ruleErrImpl) Code() string {
	return re.code
}

func (re *ruleErrImpl) Error() string {
	return re.msg
}
