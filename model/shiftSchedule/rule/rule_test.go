package rule

import (
	"remora-go/model/shiftSchedule/dao"
	"remora-go/model/shiftSchedule/ds"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_CheckTowWeekRule_Err(t *testing.T) {
	ds, err := ds.NewYamlDS("../test/week2_err_test1.yml")
	assert.Nil(t, err)
	start, _ := time.Parse(time.RFC3339, "2021-05-01T08:00:00+08:00")
	end, _ := time.Parse(time.RFC3339, "2021-05-31T08:00:00+08:00")
	ss := ds.GetScheduleByOwner("peter", start, end)

	openDate, _ := time.Parse(time.RFC3339, "2021-05-03T00:00:00+08:00")
	twr := TwoWeekRule{
		Start: openDate,
	}
	err = twr.CheckOfficialHoliday(ss)
	assert.NotNil(t, err)
	for _, s := range ss {
		if s.HasErr() {
			if s.RuleErr.Code() == ErrorOfficialHolidayNotEnouph {
				assert.Equal(t, 24, s.Start.Day())
			}
		}
	}

	err = twr.CheckOffDay(ss)
	assert.NotNil(t, err)

	for _, s := range ss {
		if s.HasErr() {
			if s.RuleErr.Code() == ErrorOffDayNotEnouph {
				assert.Equal(t, 16, s.Start.Day())
			}
		}
	}

	err = twr.CheckDailyHour(ss)
	assert.NotNil(t, err)

	var errSS dao.ShiftSchedule
	for _, s := range ss {
		if s.HasErr() {
			if s.RuleErr.Code() == ErrorDailyWorkHours {
				errSS.Add(s)
			}
		}
	}
	assert.Equal(t, 2, len(errSS))

	err = twr.CheckWeekHour(ss)
	assert.NotNil(t, err)
	for _, s := range ss {
		if s.HasErr() {
			if s.RuleErr.Code() == ErrorOverWeekWorkHours {
				assert.Equal(t, 14, s.Start.Day())
			}
		}
	}
}

func Test_CheckTowWeekRule_Normal(t *testing.T) {
	sds, err := ds.NewYamlDS("../test/week2_normal_test1.yml")
	assert.Nil(t, err)
	start, _ := time.Parse(time.RFC3339, "2021-05-01T08:00:00+08:00")
	end, _ := time.Parse(time.RFC3339, "2021-05-31T08:00:00+08:00")
	ss := sds.GetScheduleByOwner("peter", start, end)

	openDate, _ := time.Parse(time.RFC3339, "2021-05-03T00:00:00+08:00")
	twr := TwoWeekRule{
		Start: openDate,
	}
	err = twr.CheckOfficialHoliday(ss)
	assert.Nil(t, err)

	err = twr.CheckOffDay(ss)
	assert.Nil(t, err)

	err = twr.CheckDailyHour(ss)
	assert.Nil(t, err)

	err = twr.CheckWeekHour(ss)
	assert.Nil(t, err)

	sds, err = ds.NewYamlDS("../test/week2_normal_test2.yml")
	ss = sds.GetScheduleByOwner("peter", start, end)
	assert.Nil(t, err)

	err = twr.CheckOfficialHoliday(ss)
	assert.Nil(t, err)

	err = twr.CheckOffDay(ss)
	assert.Nil(t, err)

	err = twr.CheckDailyHour(ss)
	assert.Nil(t, err)

	err = twr.CheckWeekHour(ss)
	assert.Nil(t, err)
}
