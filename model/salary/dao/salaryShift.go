package dao

import "remora-go/model/shiftSchedule/dao"

type SalaryShift struct {
	dao.Shift
	// 加班費倍率
	Multiple float64
}

type SalaryShiftSchedule []*SalaryShift
