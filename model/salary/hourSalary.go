package salary

import (
	"remora-go/model/salary/dao"
	scheDao "remora-go/model/shiftSchedule/dao"
)

type hourSalary struct {
	commonSalary
}

func newHourSalary(hourPay float64) Salary {
	return &hourSalary{
		commonSalary: commonSalary{
			hourPay: hourPay,
		},
	}
}

func (hs *hourSalary) SetShiftSchedule(ss scheDao.ShiftSchedule) {

}

// 取得加班清單
func (hs *hourSalary) GetOvertimeSchedule() []*dao.SalaryShift {

	return nil
}
