package dao

import (
	"math"
)

// 勞保被保險人類別
type LaborInsType string

const (
	// 有一定雇主員工參加就業保險
	LaborInsTypeEmployeeWithEmploymentIns = LaborInsType("employeeWithEI")
	// 有一定雇主員工不參加就業保險
	LaborInsTypeEmployeeNoEmploymentIns = LaborInsType("employeeNoEI")
	// 雇主
	LaborInsTypeHirer = LaborInsType("hirer")
	// 職訓機構受訓人員
	LaborInsTypeTrainee = LaborInsType("trainee")
	// 職業工會會員
	LaborInsTypeUnion = LaborInsType("union")
	// 漁會甲類會員
	LaborInsTypeFisherAssnMember = LaborInsType("fisherAssnMember")
	// 漁民上岸候船
	LaborInsTypeFisherWaiting = LaborInsType("fisherWaiting")
	// 外僱船員
	LaborInsTypeForeignFisher = LaborInsType("foreignFisher")
	// 外僱船員上岸候船
	LaborInsTypeForeignFisherWaiting = LaborInsType("foreignFisherWaiting")
	// 已領老年給付自願加職災保險人員
	LaborInsTypeRetiredJoinInjuryIns = LaborInsType("retiredJoinInjuryIns")
	// 被裁減資遣續保人員
	LaborInsTypeLayoffRenewer = LaborInsType("layoffRenewer")
	// 育嬰留職停薪續保人員
	LaborInsTypeLeaveOfAbsenceRenewer = LaborInsType("leaveOfAbsenceRenewer")
	// 職災醫療期間退保續保人員
	LaborInsTypePeriodOfMedicalCareRenewer = LaborInsType("periodOfMedicalCareRenewer")
	// 二年以上職災醫療期間退保續保人員
	LaborInsTypeTwoYearPeriodOfMedicalCareRenewer = LaborInsType("twoYearPeriodOfMedicalCareRenewer")
	// 僅參加就業保險人員
	LaborInsTypeOnlyEmploymentIns = LaborInsType("onlyEI")
	// 庇護性就業身心障礙被保險人
	LaborInsTypeSheltered = LaborInsType("sheltered")
)

// 身心障礙等級
type DisabilityLevel string

const (
	// 無身心障礙
	DisabilityLevelNone = DisabilityLevel("none")
	// 輕度
	DisabilityLevelMild = DisabilityLevel("mild")
	// 中度
	DisabilityLevelModerate = DisabilityLevel("moderate")
	// 重度
	DisabilityLevelSevere = DisabilityLevel("severe")
	// 極重度
	DisabilityLevelExtremelySevere = DisabilityLevel("extremelySevere")
)

type LaborIns struct {
	Year int
	Desc string
	// 意外事故保險比例
	AccRate float64 `json:"accidentRate"`
	// 就業保險比例
	EmployRate float64 `json:"employmentRate"`
	// 分擔比例
	ShareMap map[LaborInsType]*laborShareDetail
	// 投保級距
	Grade []int
	// 殘障補助比例
	DisabilitySubsidyRate map[DisabilityLevel]float64
}

type laborShareDetail struct {
	Injury     []float64
	Accident   []float64
	Employment []float64
}

type LaborInsCalResult struct {
	// 投保金額
	InsuredAmount int `json:"insuredAmount"`
	// 勞工負擔
	Labor laborInsDetail `json:"labor"`
	// 單位負擔
	Organ laborInsDetail `json:"organ"`
	// 政府負擔
	Gov laborInsDetail `json:"gov"`
}

type laborInsDetail struct {
	// 保險普通事故保險費
	AccidentIns int `json:"accidentIns"`
	// 職業災害保險費
	InjuryIns int `json:"injuryIns"`
	// 就業保險費
	EmployeementIns int `json:"employeementIns"`
}

func (l *LaborIns) Calculate(salary int, laborTyp LaborInsType, dl DisabilityLevel, injuryRate float64, days uint8) *LaborInsCalResult {
	insured := l.getGrade(salary)
	dsr, _ := l.DisabilitySubsidyRate[dl]
	result := &LaborInsCalResult{
		InsuredAmount: insured,
	}
	share := l.ShareMap[laborTyp]
	var labor, organ, gov int
	labor, organ, gov = l.getAccident(insured, days, dsr, share.Accident)
	result.Labor.AccidentIns = labor
	result.Organ.AccidentIns = organ
	result.Gov.AccidentIns = gov

	labor, organ, gov = l.getEmployee(insured, days, dsr, share.Employment)
	result.Labor.EmployeementIns = labor
	result.Organ.EmployeementIns = organ
	result.Gov.EmployeementIns = gov

	labor, organ, gov = l.getInjury(insured, days, dsr, injuryRate, share.Injury)
	result.Labor.InjuryIns = labor
	result.Organ.InjuryIns = organ
	result.Gov.InjuryIns = gov
	// switch laborTyp {
	// case LaborInsTypeEmployeeWithEmploymentIns, LaborInsTypeSheltered:
	// 	labor, organ, gov = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ

	// 	labor, organ, gov = l.getEmployee(insured, days, dsr)
	// 	result.Labor.EmployeementIns = labor
	// 	result.Organ.EmployeementIns = organ

	// 	labor, organ, gov = l.getInjury(insured, days, dsr, injuryRate)
	// 	result.Labor.InjuryIns = labor
	// 	result.Organ.InjuryIns = organ
	// case LaborInsTypeEmployeeNoEmploymentIns, LaborInsTypeHirer, LaborInsTypeTrainee:
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ

	// 	labor, organ = l.getInjury(insured, days, dsr, injuryRate)
	// 	result.Labor.InjuryIns = labor
	// 	result.Organ.InjuryIns = organ
	// case LaborInsTypeUnion:
	// 	// 自行負擔60%
	// 	preLaborRate, preOrganRate := l.LaborRate, l.OrganRate
	// 	l.LaborRate, l.OrganRate = 0.6, 0
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ

	// 	labor, organ = l.getInjury(insured, days, dsr, injuryRate)
	// 	result.Labor.InjuryIns = labor
	// 	result.Organ.InjuryIns = organ
	// 	l.LaborRate, l.OrganRate = preLaborRate, preOrganRate
	// case LaborInsTypeFisherAssnMember:
	// 	// 自行負擔20%，政府補助80%，單位0%
	// 	preLaborRate, preOrganRate := l.LaborRate, l.OrganRate
	// 	l.LaborRate, l.OrganRate = 0.2, 0
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ

	// 	labor, organ = l.getInjury(insured, days, dsr, injuryRate)
	// 	result.Labor.InjuryIns = labor
	// 	result.Organ.InjuryIns = organ
	// 	l.LaborRate, l.OrganRate = preLaborRate, preOrganRate
	// case LaborInsTypeFisherWaiting, LaborInsTypeForeignFisherWaiting:
	// 	// 自行全負擔
	// 	preLaborRate, preOrganRate := l.LaborRate, l.OrganRate
	// 	l.LaborRate, l.OrganRate = 1, 0
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ
	// 	l.LaborRate, l.OrganRate = preLaborRate, preOrganRate
	// case LaborInsTypeForeignFisher:
	// 	// 自行負擔80%，政府補助20%，單位0%
	// 	preLaborRate, preOrganRate := l.LaborRate, l.OrganRate
	// 	l.LaborRate, l.OrganRate = 0.8, 0
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ

	// 	labor, organ = l.getInjury(insured, days, dsr, injuryRate)
	// 	result.Labor.InjuryIns = labor
	// 	result.Organ.InjuryIns = organ
	// 	l.LaborRate, l.OrganRate = preLaborRate, preOrganRate
	// case LaborInsTypeRetiredJoinInjuryIns:
	// 	labor, organ = l.getInjury(insured, days, dsr, injuryRate)
	// 	result.Labor.InjuryIns = labor
	// 	result.Organ.InjuryIns = organ
	// case LaborInsTypeLayoffRenewer:
	// 	// 資遺員工續保自行負擔80%，政府補助20%
	// 	preLaborRate, preOrganRate := l.LaborRate, l.OrganRate
	// 	l.LaborRate, l.OrganRate = 0.8, 0
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ
	// 	l.LaborRate, l.OrganRate = preLaborRate, preOrganRate
	// case LaborInsTypeLeaveOfAbsenceRenewer:
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ

	// 	labor, organ = l.getEmployee(insured, days, dsr)
	// 	result.Labor.EmployeementIns = labor
	// 	result.Organ.EmployeementIns = organ
	// case LaborInsTypePeriodOfMedicalCareRenewer:
	// 	// 初次辦理職災醫療期間退保續保人員，於辦理初次加保生效之日起2年內，
	// 	// 其保險費由被保險人負擔20%，政府專款負擔80%。
	// 	// 2年後則由被保險人及專款各負擔50%。
	// 	preLaborRate, preOrganRate := l.LaborRate, l.OrganRate
	// 	l.LaborRate, l.OrganRate = 0.2, 0
	// 	labor, organ = l.getAccident(insured, days, dsr)
	// 	result.Labor.AccidentIns = labor
	// 	result.Organ.AccidentIns = organ
	// 	l.LaborRate, l.OrganRate = preLaborRate, preOrganRate
	// case LaborInsTypeOnlyEmploymentIns:
	// 	labor, organ = l.getEmployee(insured, days, dsr)
	// 	result.Labor.EmployeementIns = labor
	// 	result.Organ.EmployeementIns = organ
	// }
	return result
}

// 取得投保階距
func (l *LaborIns) getGrade(salary int) int {
	gl := len(l.Grade)
	for i := 0; i < gl; i++ {
		if salary <= l.Grade[i] {
			return l.Grade[i]
		}
	}
	return l.Grade[gl-1]
}

const maxDays = float64(30)

// 回傳員工自負額(labor)及單位負擔(organ)的意外事故保險金
func (l *LaborIns) getAccident(grade int, days uint8, dsr float64, share []float64) (labor, organ, gov int) {
	labor = int(math.Round(float64(grade*int(days)) / maxDays * share[0] * (1 - dsr) * l.AccRate / 100))
	organ = int(math.Round(float64(grade*int(days)) / maxDays * share[1] * l.AccRate / 100))
	gov = int(math.Round(float64(grade*int(days)) / maxDays * share[2] * l.AccRate / 100))
	return
}

// 回傳員工自負額(labor)及單位負擔(organ)的就業保險金
func (l *LaborIns) getEmployee(grade int, days uint8, dsr float64, share []float64) (labor, organ, gov int) {
	labor = int(math.Round(float64(grade*int(days)) / maxDays * share[0] * (1 - dsr) * l.EmployRate / 100))
	organ = int(math.Round(float64(grade*int(days)) / maxDays * share[1] * l.EmployRate / 100))
	gov = int(math.Round(float64(grade*int(days)) / maxDays * share[2] * l.EmployRate / 100))
	return
}

// 回傳員工自負額(labor)及單位負擔(organ)的職業災害保險金
func (l *LaborIns) getInjury(grade int, days uint8, dsr, injuryRate float64, share []float64) (labor, organ, gov int) {
	labor = int(math.Round(float64(grade*int(days)) / maxDays * share[0] * (1 - dsr) * injuryRate / 100))
	organ = int(math.Round(float64(grade*int(days)) / maxDays * share[1] * injuryRate / 100))
	gov = int(math.Round(float64(grade*int(days)) / maxDays * share[2] * injuryRate / 100))
	return
}
