package dao

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	lab = LaborIns{
		Year:       110,
		Desc:       "test",
		AccRate:    10.5,
		EmployRate: 1,
		ShareMap: map[LaborInsType]*laborShareDetail{
			LaborInsTypeEmployeeWithEmploymentIns: {
				Accident:   []float64{0.2, 0.7, 0.1},
				Employment: []float64{0.2, 0.7, 0.1},
				Injury:     []float64{0, 1, 0},
			},
			LaborInsTypeUnion: {
				Accident:   []float64{0.6, 0, 0.4},
				Employment: []float64{0, 0, 0},
				Injury:     []float64{0.6, 0, 0.4},
			},
			LaborInsTypeHirer: {
				Accident:   []float64{0.2, 0.7, 0.1},
				Employment: []float64{0, 0, 0},
				Injury:     []float64{0, 1, 0},
			},
			LaborInsTypeLeaveOfAbsenceRenewer: {
				Accident:   []float64{0.2, 0.7, 0.1},
				Employment: []float64{0.2, 0.7, 0.1},
				Injury:     []float64{0, 0, 0},
			},
			LaborInsTypeForeignFisher: {
				Accident:   []float64{0.8, 0, 0.2},
				Employment: []float64{0, 0, 0},
				Injury:     []float64{0.8, 0, 0.2},
			},
			LaborInsTypeSheltered: {
				Accident:   []float64{0.2, 0.7, 0.1},
				Employment: []float64{0.2, 0.7, 0.1},
				Injury:     []float64{0, 1, 0},
			},
		},
		Grade: []int{24000, 25200, 26400, 27600, 28800, 30300, 31800, 33300, 34800, 36300, 38200, 40100, 42000, 43900, 45800},
		DisabilitySubsidyRate: map[DisabilityLevel]float64{
			DisabilityLevelMild:            0.25,
			DisabilityLevelModerate:        0.5,
			DisabilityLevelSevere:          1,
			DisabilityLevelExtremelySevere: 1,
		},
	}
)

func Test_Calculate(t *testing.T) {
	result := lab.Calculate(23800, LaborInsTypeEmployeeWithEmploymentIns, DisabilityLevelMild, 0.12, 25)
	assert.Equal(t, 24000, result.InsuredAmount)
	assert.Equal(t, 1470, result.Organ.AccidentIns)
	assert.Equal(t, 24, result.Organ.InjuryIns)
	assert.Equal(t, 140, result.Organ.EmployeementIns)
	assert.Equal(t, 315, result.Labor.AccidentIns)
	assert.Equal(t, 0, result.Labor.InjuryIns)
	assert.Equal(t, 30, result.Labor.EmployeementIns)

	result = lab.Calculate(23800, LaborInsTypeUnion, DisabilityLevelModerate, 0.12, 25)
	assert.Equal(t, 24000, result.InsuredAmount)
	assert.Equal(t, 0, result.Organ.AccidentIns)
	assert.Equal(t, 0, result.Organ.InjuryIns)
	assert.Equal(t, 0, result.Organ.EmployeementIns)
	assert.Equal(t, 630, result.Labor.AccidentIns)
	assert.Equal(t, 7, result.Labor.InjuryIns)
	assert.Equal(t, 0, result.Labor.EmployeementIns)

	result = lab.Calculate(23800, LaborInsTypeHirer, DisabilityLevelModerate, 0.12, 15)
	assert.Equal(t, 24000, result.InsuredAmount)
	assert.Equal(t, 882, result.Organ.AccidentIns)
	assert.Equal(t, 14, result.Organ.InjuryIns)
	assert.Equal(t, 0, result.Organ.EmployeementIns)
	assert.Equal(t, 126, result.Labor.AccidentIns)
	assert.Equal(t, 0, result.Labor.InjuryIns)
	assert.Equal(t, 0, result.Labor.EmployeementIns)
	assert.Equal(t, 126, result.Gov.AccidentIns)
	assert.Equal(t, 0, result.Gov.InjuryIns)
	assert.Equal(t, 0, result.Gov.EmployeementIns)

	result = lab.Calculate(23800, LaborInsTypeLeaveOfAbsenceRenewer, DisabilityLevelMild, 0.12, 15)
	assert.Equal(t, 24000, result.InsuredAmount)
	assert.Equal(t, 882, result.Organ.AccidentIns)
	assert.Equal(t, 0, result.Organ.InjuryIns)
	assert.Equal(t, 84, result.Organ.EmployeementIns)
	assert.Equal(t, 189, result.Labor.AccidentIns)
	assert.Equal(t, 0, result.Labor.InjuryIns)
	assert.Equal(t, 18, result.Labor.EmployeementIns)
	assert.Equal(t, 126, result.Gov.AccidentIns)
	assert.Equal(t, 0, result.Gov.InjuryIns)
	assert.Equal(t, 12, result.Gov.EmployeementIns)

	result = lab.Calculate(23800, LaborInsTypeForeignFisher, DisabilityLevelNone, 0.12, 29)
	assert.Equal(t, 24000, result.InsuredAmount)
	assert.Equal(t, 0, result.Organ.AccidentIns)
	assert.Equal(t, 0, result.Organ.InjuryIns)
	assert.Equal(t, 0, result.Organ.EmployeementIns)
	assert.Equal(t, 1949, result.Labor.AccidentIns)
	assert.Equal(t, 22, result.Labor.InjuryIns)
	assert.Equal(t, 0, result.Labor.EmployeementIns)
	assert.Equal(t, 487, result.Gov.AccidentIns)
	assert.Equal(t, 6, result.Gov.InjuryIns)
	assert.Equal(t, 0, result.Gov.EmployeementIns)

	result = lab.Calculate(23800, LaborInsTypeSheltered, DisabilityLevelSevere, 0.12, 30)
	assert.Equal(t, 24000, result.InsuredAmount)
	assert.Equal(t, 1764, result.Organ.AccidentIns)
	assert.Equal(t, 29, result.Organ.InjuryIns)
	assert.Equal(t, 168, result.Organ.EmployeementIns)
	assert.Equal(t, 0, result.Labor.AccidentIns)
	assert.Equal(t, 0, result.Labor.InjuryIns)
	assert.Equal(t, 0, result.Labor.EmployeementIns)
	assert.Equal(t, 252, result.Gov.AccidentIns)
	assert.Equal(t, 0, result.Gov.InjuryIns)
	assert.Equal(t, 24, result.Gov.EmployeementIns)
}

func Test_GetGrade(t *testing.T) {
	grade := lab.getGrade(43000)
	assert.Equal(t, 43900, grade)
	grade = lab.getGrade(25200)
	assert.Equal(t, 25200, grade)
}

func Test_GetAccident(t *testing.T) {
	grade := lab.getGrade(30000)
	assert.Equal(t, 30300, grade)
	labor, organ, _ := lab.getAccident(grade, 30, 0, []float64{0.2, 0.7, 0.1})
	assert.Equal(t, 636, labor)
	assert.Equal(t, 2227, organ)

	grade = lab.getGrade(33000)
	assert.Equal(t, 33300, grade)
	labor, organ, _ = lab.getAccident(grade, 23, 0, []float64{0.2, 0.7, 0.1})
	assert.Equal(t, 536, labor)
	assert.Equal(t, 1876, organ)
}

func Test_GetEmployee(t *testing.T) {
	grade := lab.getGrade(30000)
	assert.Equal(t, 30300, grade)
	labor, organ, _ := lab.getEmployee(grade, 30, 0, []float64{0.2, 0.7, 0.1})
	assert.Equal(t, 61, labor)
	assert.Equal(t, 212, organ)

	grade = lab.getGrade(35000)
	assert.Equal(t, 36300, grade)
	labor, organ, _ = lab.getEmployee(grade, 10, 0, []float64{0.2, 0.7, 0.1})
	assert.Equal(t, 24, labor)
	assert.Equal(t, 85, organ)

}

var (
	health = HealthIns{
		Year:                110,
		HealthRate:          5.17,
		AverageOfHouseholds: 0.58,
		AverageInsurance:    1377,
		Grade: []int{24000, 25200, 26400, 27600, 28800, 30300,
			31800, 33300, 34800, 36300, 38200, 40100, 42000, 43900,
			45800, 50600, 53000, 55400, 57800},
		ShareMap: map[HealthInsType]*healthShareDetail{
			HealthInsTypePublicOfficials: {
				Self:       []float64{0.3, 0.7, 0},
				Households: []float64{0.3, 0.7, 0},
			},
			HealthInsTypeEmployee: {
				Self:       []float64{0.3, 0.6, 0.1},
				Households: []float64{0.3, 0.6, 0.1},
			},
			HealthInsTypeSecond: {
				Self:       []float64{0.6, 0, 0.4},
				Households: []float64{0.6, 0, 0.4},
			},
			HealthInsTypeHirer: {
				Self:       []float64{1, 0, 0},
				Households: []float64{1, 0, 0},
			},
			HealthInsTypePrivateSchool: {
				Self:       []float64{0.3, 0.35, 0.35},
				Households: []float64{0.3, 0.35, 0.35},
			},
			HealthInsTypeThird: {
				Self:       []float64{0.3, 0, 0.7},
				Households: []float64{0.3, 0, 0.7},
			},
			HealthInsTypeFourth: {
				Self: []float64{0, 0, 1},
			},
			HealthInsTypeFifth: {
				Self: []float64{0, 0, 1},
			},
			HealthInsTypeVeterans: {
				Self:       []float64{0, 0, 1},
				Households: []float64{0.3, 0, 0.7},
			},
			HealthInsTypeOther: {
				Self:       []float64{0.6, 0, 0.4},
				Households: []float64{0.6, 0, 0.4},
			},
		},
		SubsidyRate: map[SubsidyObject]float64{
			SubsidyDisablilityMild:    0.25,
			SubsidyDisabilityModerate: 0.5,
			SubsidyDisabilitySevere:   1,
		},
	}
)

func Test_HealthCalculate(t *testing.T) {
	// 一般員工無眷屬
	result := health.Calculate(&CalInput{
		Salary: 52900,
		Typ:    HealthInsTypeEmployee,
	})
	assert.Equal(t, 53000, result.Insured)
	assert.Equal(t, 822, result.Labor.Insurance)
	assert.Equal(t, 2598, result.Organ)

	// 一般員工二個眷屬無補助
	result = health.Calculate(&CalInput{
		Salary:            52900,
		Typ:               HealthInsTypeEmployee,
		HouseholdsSubsidy: []SubsidyObject{SubsidyNone, SubsidyNone},
	})
	assert.Equal(t, 53000, result.Insured)
	assert.Equal(t, 2466, result.Labor.Insurance)
	assert.Equal(t, 2598, result.Organ)

	// 職業工會無眷屬
	result = health.Calculate(&CalInput{
		Salary: 52900,
		Typ:    HealthInsTypeSecond,
	})
	assert.Equal(t, 53000, result.Insured)
	assert.Equal(t, 1644, result.Labor.Insurance)
	assert.Equal(t, 0, result.Organ)

	// 職業工會二個眷屬
	result = health.Calculate(&CalInput{
		Salary:            52900,
		Typ:               HealthInsTypeSecond,
		HouseholdsSubsidy: []SubsidyObject{SubsidyNone, SubsidyNone},
	})
	assert.Equal(t, 53000, result.Insured)
	assert.Equal(t, 4932, result.Labor.Insurance)
	assert.Equal(t, 0, result.Organ)

	// 雇主、專門職業技術人員自行執業者，無眷口
	result = health.Calculate(&CalInput{
		Salary: 56900,
		Typ:    HealthInsTypeHirer,
	})
	assert.Equal(t, 57800, result.Insured)
	assert.Equal(t, 2988, result.Labor.Insurance)
	assert.Equal(t, 0, result.Organ)

	// 雇主、專門職業技術人員自行執業者，1眷口
	result = health.Calculate(&CalInput{
		Salary:            56900,
		Typ:               HealthInsTypeHirer,
		HouseholdsSubsidy: []SubsidyObject{SubsidyNone},
	})
	assert.Equal(t, 57800, result.Insured)
	assert.Equal(t, 5976, result.Labor.Insurance)
	assert.Equal(t, 0, result.Organ)

	// 私校教職員，4眷口
	result = health.Calculate(&CalInput{
		Salary:            56900,
		Typ:               HealthInsTypePrivateSchool,
		HouseholdsSubsidy: []SubsidyObject{SubsidyNone, SubsidyNone, SubsidyNone, SubsidyNone},
	})
	assert.Equal(t, 57800, result.Insured)
	assert.Equal(t, 3584, result.Labor.Insurance)
	assert.Equal(t, 1653, result.Organ)

	// 榮民無眷口
	result = health.Calculate(&CalInput{
		Typ: HealthInsTypeVeterans,
	})
	assert.Equal(t, 0, result.Labor.Insurance)
	assert.Equal(t, 0, result.Organ)

	// 榮民二眷口
	result = health.Calculate(&CalInput{
		Typ:               HealthInsTypeVeterans,
		HouseholdsSubsidy: []SubsidyObject{SubsidyNone, SubsidyNone},
	})
	assert.Equal(t, 826, result.Labor.Insurance)
	assert.Equal(t, 0, result.Organ)

	// 地區人口，二眷口
	result = health.Calculate(&CalInput{
		Typ:               HealthInsTypeOther,
		HouseholdsSubsidy: []SubsidyObject{SubsidyNone, SubsidyNone},
	})
	assert.Equal(t, 2478, result.Labor.Insurance)
	assert.Equal(t, 0, result.Organ)
}

func Test_HealthCalculateSubsidy(t *testing.T) {
	// 一般員工(補助1/2)無眷屬
	result := health.Calculate(&CalInput{
		Salary:  23400,
		Typ:     HealthInsTypeEmployee,
		Subsidy: SubsidyDisabilityModerate,
	})
	assert.Equal(t, 24000, result.Insured)
	assert.Equal(t, 372, result.Labor.Insurance)
	assert.Equal(t, 186, result.Labor.SubsidyAmount)
	assert.Equal(t, 1176, result.Organ)

	// 一般員工(補助1/2)、1眷屬(補助1/4)
	result = health.Calculate(&CalInput{
		Salary:            23400,
		Typ:               HealthInsTypeEmployee,
		Subsidy:           SubsidyDisabilityModerate,
		HouseholdsSubsidy: []SubsidyObject{SubsidyDisablilityMild},
	})
	assert.Equal(t, 24000, result.Insured)
	assert.Equal(t, 744, result.Labor.Insurance)
	assert.Equal(t, 279, result.Labor.SubsidyAmount)
	assert.Equal(t, 1176, result.Organ)

	// 一般員工(補助1/4)、4眷屬(補助1/2)
	result = health.Calculate(&CalInput{
		Salary:            23400,
		Typ:               HealthInsTypeEmployee,
		Subsidy:           SubsidyDisablilityMild,
		HouseholdsSubsidy: []SubsidyObject{SubsidyDisabilityModerate, SubsidyDisabilityModerate, SubsidyDisabilityModerate, SubsidyDisabilityModerate},
	})
	assert.Equal(t, 24000, result.Insured)
	assert.Equal(t, 1488, result.Labor.Insurance)
	assert.Equal(t, 651, result.Labor.SubsidyAmount)
	assert.Equal(t, 1176, result.Organ)

	// 一般員工(補助1/4)、1眷屬(補助1/4*1、1/2*3、1*1)
	result = health.Calculate(&CalInput{
		Salary:  23400,
		Typ:     HealthInsTypeEmployee,
		Subsidy: SubsidyDisablilityMild,
		HouseholdsSubsidy: []SubsidyObject{SubsidyDisabilityModerate, SubsidyDisabilityModerate,
			SubsidyDisabilityModerate, SubsidyDisabilitySevere, SubsidyDisablilityMild},
	})
	assert.Equal(t, 24000, result.Insured)
	assert.Equal(t, 1488, result.Labor.Insurance)
	assert.Equal(t, 837, result.Labor.SubsidyAmount)
	assert.Equal(t, 1176, result.Organ)

	// 一般員工(補助1/4)、1眷屬(補助1/4*1、1/2*3、1*3)
	result = health.Calculate(&CalInput{
		Salary:  23400,
		Typ:     HealthInsTypeEmployee,
		Subsidy: SubsidyDisablilityMild,
		HouseholdsSubsidy: []SubsidyObject{SubsidyDisabilityModerate, SubsidyDisabilityModerate,
			SubsidyDisabilityModerate, SubsidyDisabilitySevere, SubsidyDisablilityMild,
			SubsidyDisabilitySevere, SubsidyDisabilitySevere},
	})
	assert.Equal(t, 24000, result.Insured)
	assert.Equal(t, 1488, result.Labor.Insurance)
	assert.Equal(t, 1209, result.Labor.SubsidyAmount)
	assert.Equal(t, 1176, result.Organ)
}
