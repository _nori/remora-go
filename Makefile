gcp:
	gcloud config set project raccoon-297605
	gcloud app deploy -v $(V)

run: build
	./bin/$(SER) -confpath ./conf

build: clear
	go build ${LDFLAGS} -o ./bin/$(SER) ./service/$(SER)/main.go

clear:
	rm -rf ./bin/$(SER)