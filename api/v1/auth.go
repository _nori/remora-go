package v1

import (
	"fmt"
	"net/http"
	"remora-go/core"
	"remora-go/core/api"
	"remora-go/core/log"
	"remora-go/util"
)

func NewAuthAPI(l log.Logger) api.API {
	return &authAPI{
		l: l,
	}
}

type authAPI struct {
	l log.Logger
}

func (app *authAPI) GetName() string {
	return "auth"
}

func (app *authAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		{Path: "/v1/login", Next: app.loginEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/token", Next: app.tokenEndpoint, Method: "GET", Auth: true},
	}
}

func (app *authAPI) loginEndpoint(w http.ResponseWriter, req *http.Request) {
	di := core.GetDI()
	authURL := di.GetAuthUrl(fmt.Sprintf("http://%s/v1/token", req.Host))
	http.Redirect(w, req, authURL, http.StatusTemporaryRedirect)
}

func (app *authAPI) tokenEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"code"}, true)
	fmt.Println(qv)
	di := core.GetDI()
	t, err := di.GetAccessToken(qv["code"].(string), fmt.Sprintf("http://%s/v1/token", req.Host))
	fmt.Println(t, err)
	a, err := di.GetUserInfo(t)
	fmt.Println(a, err)
	w.Write([]byte("token"))
}
