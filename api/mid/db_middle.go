package mid

import (
	"net/http"
	"runtime"

	"remora-go/core"
	"remora-go/core/api/mid"
	"remora-go/core/db"
	"remora-go/core/log"
	"remora-go/util"

	"github.com/google/uuid"
)

type DBMiddle string

func NewDBMid(name string) mid.Middle {
	return &dbMiddle{
		name: name,
	}
}

type dbMiddle struct {
	name string
}

func (lm *dbMiddle) GetName() string {
	return lm.name
}

func (am *dbMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			uuid := uuid.New().String()
			di := core.GetDI()
			l := di.NewLogger(uuid)

			dbclt, err := di.NewMongoDBClient(r.Context(), "")
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}

			r = util.SetCtxKeyVal(r, db.CtxMongoKey, dbclt)

			r = util.SetCtxKeyVal(r, log.CtxLogKey, l)
			f(w, r)
			dbclt.Close()
			runtime.GC()
		}
	}
}
