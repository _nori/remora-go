package main

import (
	"bufio"
	"bytes"
	"fmt"
	"image/png"
	"io/ioutil"
	"os"

	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"

	"go.mongodb.org/mongo-driver/bson"
)

func display(key *otp.Key, data []byte) {
	fmt.Printf("Issuer:       %s\n", key.Issuer())
	fmt.Printf("Account Name: %s\n", key.AccountName())
	fmt.Printf("Secret:       %s\n", key.Secret())
	fmt.Println("Writing PNG to qr-code.png....")
	ioutil.WriteFile("qr-code.png", data, 0644)
	fmt.Println("")
	fmt.Println("Please add your TOTP to your OTP Application now!")
	fmt.Println("")
}

type comMonitor struct {
	DB        string     `bson:"$db"`
	Insert    string     `bson:"insert"`
	Documents []bson.Raw `bson:"documents"`
}

func promptForPasscode() string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter Passcode: ")
	text, _ := reader.ReadString('\n')
	return text
}

func main() {
	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      "Example.com",
		AccountName: "alice@example.com",
		Secret:      []byte("FIW6ZGWGLJPLD56JFKBHFRHNBSVF2D24"),
	})
	if err != nil {
		panic(err)
	}
	// Convert TOTP key into a PNG
	var buf bytes.Buffer
	img, err := key.Image(200, 200)
	if err != nil {
		panic(err)
	}
	png.Encode(&buf, img)

	// display the QR code to the user.
	display(key, buf.Bytes())

	passcode := promptForPasscode()
	valid := totp.Validate(passcode, key.Secret())
	if valid {
		println("Valid passcode!")
		os.Exit(0)
	} else {
		println("Invalid passcode!")
		os.Exit(1)
	}
	// startedCommands := make(map[int64]bson.Raw)
	// cmdMonitor := &event.CommandMonitor{
	// 	Started: func(_ context.Context, evt *event.CommandStartedEvent) {
	// 		startedCommands[evt.RequestID] = evt.Command
	// 	},
	// 	Succeeded: func(_ context.Context, evt *event.CommandSucceededEvent) {
	// 		if evt.Reply.Lookup("ok").Double() == 1.0 {
	// 			cm := comMonitor{}
	// 			c := startedCommands[evt.RequestID]
	// 			log.Println(c, evt)
	// 			err := bson.Unmarshal(c, &cm)
	// 			fmt.Println(err, cm)
	// 		}

	// 	},
	// 	Failed: func(_ context.Context, evt *event.CommandFailedEvent) {
	// 		log.Printf("Command: %v Failure: %v\n",
	// 			startedCommands[evt.RequestID],
	// 			evt.Failure,
	// 		)
	// 	},
	// }
	// ctx := context.Background()
	// clientOpts := options.Client().ApplyURI("mongodb://localhost:27017").SetMonitor(cmdMonitor)
	// client, err := mongo.Connect(ctx, clientOpts)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// defer func() {
	// 	if err = client.Disconnect(context.TODO()); err != nil {
	// 		log.Fatal(err)
	// 	}
	// }()
	// time.Sleep(time.Minute)
}
