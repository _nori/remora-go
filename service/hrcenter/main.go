package main

import (
	"flag"
	"fmt"

	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"

	"remora-go/core"
	"remora-go/core/log"

	"remora-go/api/mid"
	v1 "remora-go/api/v1"
	coremid "remora-go/core/api/mid"
)

var (
	confpath = flag.String("confpath", "", "assign conf path")
	v        = flag.Bool("v", false, "version")

	Version   = "1.0.0"
	BuildTime = "2000-01-01T00:00:00+0800"
)

func main() {

	flag.Parse()

	if *v {
		fmt.Println(Version)
		return
	}

	if *confpath == "" {
		exePath, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			panic(err)
		}
		*confpath = exePath
	}

	err := godotenv.Load(*confpath + "/.env")
	if err != nil {
		fmt.Println("No .env file")
	}

	env := os.Getenv("ENV")
	if env == "" {
		core.InitDefaultConf(*confpath)
	} else {
		core.IniConfByEnv(*confpath, env)
	}
	di := core.GetDI()
	log := di.NewLogger("hrcenter")

	runAPI(di, log)
}

func runAPI(apiConf core.DI, log log.Logger) {
	router := mux.NewRouter()
	fmt.Println("run api")
	// authMiddle := middle.NewAuthMid(kid, log)
	apiConf.InitAPI(
		router,
		[]coremid.Middle{
			//mid.NewDBMid("db"),
			mid.NewDebugMid("debug"),
			// authMiddle,
		},
		nil,
		//	authMiddle,
		v1.NewLaborInsAPI(log),
		v1.NewAuthAPI(log),
	)
	port := apiConf.GetPort()
	if core.IsTest() {
		log.Info(fmt.Sprintf("start api in [test] mode with port: %s", port))
	} else {
		log.Info(fmt.Sprintf("start api in [prod] mode with port: %s", port))
	}

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), router).Error())
}
